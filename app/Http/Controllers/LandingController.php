<?php

namespace App\Http\Controllers;

use Goutte\Client;
use Illuminate\Http\Request;

class LandingController extends Controller
{

    public $results = array();
    public $resultDetail = [];
    public $urls = [
        'https://www.worldometers.info/coronavirus/',
        'https://bmc.baliprov.go.id/search-news?key=jokowi',
    ];

    public function index ()
    {
//        return view('landing.index');

        $client = new Client();

        // URL NEWS PORTAL
        $page = $client->request('GET', $this->urls[1]);

        /*
         * Get data from Worldometer
         */
//        echo '<pre>';
//        print_r($page->filter('.maincounter-number')->text());


        /*
         * Get data from Worldometer
         */
//        $page->filter('#maincounter-wrap')->each(function ($node) {
//            $this->results[] = $node->text();
//        });

        /*
         * Get data from Baliprov
         */
        $page->filter('.what-cap')->filter('a')->each(function ($node) {
            $this->results[] = [
                'title' => $node->text(),
                'url' => $node->attr('href'),
            ];
        });
//        return $this->results;

        foreach ($this->results as $key => $val) {
            $clientDetail = new Client();
            $pageDetail = $clientDetail->request('GET', $val['url']);
            $pageDetail->filter('#news_isi')->each(function ($node) use ($val){
                $this->resultDetail[] = [
                    'title' => $val['title'],
                    'content' => $node->text(),
                ];
            });
        }
//        return $this->resultDetail;

        /*
         * Get data from Baliprov
         */
        $resultDetail = [];
        foreach ($this->resultDetail as $key => $val) {
            $guzzle = new \GuzzleHttp\Client();
            $resultGuzzle = $guzzle->post('https://language.googleapis.com/v1/documents:analyzeSentiment?key=' . env('GOOGLE_API_KEY'), [
                'json' => [
                    'document' => [
                        'type' => 'PLAIN_TEXT',
                        'content' => $val['content'],
                    ]
                ]
            ])->getBody()->getContents();
            $resultDetail[] = [
             'title' => $val['title'],
             'content' => $val['content'],
             'score' => json_decode($resultGuzzle)->documentSentiment->score
            ];
        }
        return $resultDetail;

        $guzzle = new \GuzzleHttp\Client();
        $resultGuzzle = $guzzle->post('https://language.googleapis.com/v1/documents:analyzeSentiment?key=' . env('GOOGLE_API_KEY'), [
            'json' => [
                'document' => [
                    'type' => 'PLAIN_TEXT',
                    'content' => $this->resultDetail[0]['content'],
                ]
            ]
        ]);
        echo '<pre>';
        return $resultGuzzle->getBody()->getContents();
    }
}
