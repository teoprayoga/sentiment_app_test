<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login (Request $request)
    {

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $user = Auth::guard('web')->attempt($request->only('email', 'password'), $request->filled('remember'));

            if (empty($user)) {
                return back()->withErrors(['email' => 'Email or password is incorrect']);
            }

            return redirect()->route('user.dashboard');
        }

        return view('user.login', [
            'data' => $request,
        ]);
    }

    public function logout (Request $request) {
        Auth::guard('web')->logout();
        return redirect()->route('user.login');
    }

    public function register (Request $request) {

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
            ]);

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            session()->flash('message', 'User registration successful');
        }

        return view('user.register');
    }

    public function dashboard (Request $request) {
        return view('user.dashboard');
    }
}
