<?php

use App\Http\Controllers\LandingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', [LandingController::class, 'index'])->name('landing.index');
Route::any('/register', [UserController::class, 'register'])->name('user.register')->middleware(['guest']);
Route::any('/login', [UserController::class, 'login'])->name('user.login')->middleware(['guest']);
Route::any('/logout', [UserController::class, 'logout'])->name('user.logout');
Route::group(['middleware' => ['auth']], function () {
    Route::any('/dashboard', [UserController::class, 'dashboard'])->name('user.dashboard');
});
